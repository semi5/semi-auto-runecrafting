export function setup(ctx) {
	ctx.onCharacterSelectionLoaded(ctx => {
		// debug
		const debugLog = (...msg) => {
			//console.debug(`[${id} v10045]`, ...msg);
		};

		// script details
		const id = 'semi-auto-runecrafting';
		const title = 'SEMI Auto Runecrafting';

		// constants
		const MODE_LINEAR = 1;
		const MODE_LOWEST_CAST = 2;
		const MODE_ENDLESS = 3;

		// game references
		const RC_SKILL = game.runecrafting;
		const RC_CATEGORY = RC_SKILL.categories.allObjects;
		const MINING_SKILL = game.mining;
		const MINING_ACTIONS = MINING_SKILL.actions;
		const MINING_RESOURCES = {};
		const MINING_RESOURCES_ARRAY = [];

		// settings
		const SETTINGS = ctx.settings.section('General');

		// helper class
		class CollapsibleMenu {
			constructor(parent, titletext) {
				this.parent = parent;
				this.isOpen = true;
				this.icon = this.createHeader(titletext);
				this.hideBlock = createElement('div');
				this.container = this.hideBlock.appendChild(createElement('div', {
					classList: ['p-3']
				})).appendChild(createElement('div', {
					classList: ['row', 'gutters-tiny', 'row-deck']
				}));
				this.parent.appendChild(this.hideBlock);
			}
			createHeader(titletext) {
				const mainBlock = createElement('div', {
					classList: ['block-header', 'block-header-default', 'pointer-enabled']
				});
				mainBlock.onclick = (event) => {
					this.toggle();
				};

				const title = createElement('h3', {
					classList: ['block-title']
				});
				title.append(createElement('i', {
					classList: ['fa', 'fa-cog', 'text-muted', 'mr-1']
				}));
				title.append(document.createTextNode(titletext));

				const options = createElement('div', {
					classList: ['block-options']
				});
				const icon = createElement('i', {
					classList: ['far', 'fa-eye']
				});
				options.append(icon);

				mainBlock.append(title, options);

				this.parent.appendChild(mainBlock);
				return icon;
			}
			toggle() {
				if (this.isOpen) {
					this.icon.classList.add('fa-eye-slash');
					this.icon.classList.remove('fa-eye');
					hideElement(this.hideBlock);
				} else {
					this.icon.classList.add('fa-eye');
					this.icon.classList.remove('fa-eye-slash');
					showElement(this.hideBlock);
				}
				this.isOpen = !this.isOpen;
			}
		}

		// generate rune options
		const defaultRatioConfig = {};
		const RUNE_ACTIONS = {};
		const RUNE_ACTIONS_ARRAY = [];

		const getMiningRequirements = (action) => {
			let miningActions = [];

			action.itemCosts.forEach((itemCost) => {
				let miningAction = MINING_ACTIONS.find((mc_action) => mc_action.product == itemCost.item);
				if (miningAction) {
					miningActions.push({
						action: miningAction,
						cost: itemCost.quantity
					});
					MINING_RESOURCES[miningAction.id] = miningAction.product;
				}
			})
			return miningActions;
		}

		game.runecrafting.actions.filter((action) => action.product.type == "Rune").forEach((action, index) => {
			RUNE_ACTIONS[action.id] = {
				id: action.id,
				category: action.category,
				recipe: action,
				product: action.product,
				weight: action.level + action.abyssalLevel + RC_CATEGORY.indexOf(action.category) * 1000,
				level: action.level,
				abyssalLevel: action.abyssalLevel,
				realm: action.realm,
				mining: getMiningRequirements(action)
			};
			defaultRatioConfig[action.id] = 0;
		});
		RUNE_ACTIONS_ARRAY.push(...Object.values(RUNE_ACTIONS));
		RUNE_ACTIONS_ARRAY.sort((a, b) => {
			return a.weight > b.weight ? 1 : -1;
		});
		MINING_RESOURCES_ARRAY.push(...Object.values(MINING_RESOURCES));
		///debugLog(RUNE_ACTIONS_ARRAY);

		// config
		let config = {};
		let hasUI = false;

		const getConfig = () => {
			let storedMenuConfig = ctx.characterStorage.getItem('config');
			if (storedMenuConfig != null) {
				// In the event a rune is add / removed, make sure the ratios have a key default.
				Object.keys(defaultRatioConfig)
					.filter((actionID) => {
						return storedMenuConfig.runeRatio[actionID] == null;
					})
					.forEach((actionID) => {
						storedMenuConfig.runeRatio[actionID] = 0;
					});

				return storedMenuConfig;
			}

			return {
				enabled: false,
				runeRatio: defaultRatioConfig,
				craftingMode: MODE_ENDLESS,
				baseCastCount: 10000
			};
		};

		const storeConfig = () => {
			ctx.characterStorage.setItem('config', config);
		};

		// util helpers
		const bankQty = (item) => game.bank.getQty(item);
		const bankHasRoom = (item) => bankQty(item) > 0 || game.bank.occupiedSlots < game.bank.maximumSlots;

		const getSelectedAction = () => game.runecrafting.isActive ? game.runecrafting.selectedRecipe : null;
		const runesCast = (item, actionID) => {
			return config.runeRatio[actionID] > 0 ?
				Math.floor(bankQty(item) / config.runeRatio[actionID]) :
				-1;
		};
		const runesCastFormat = (item, actionID) => {
			let castAmount = runesCast(item, actionID);
			return castAmount < 0 ? '-' : numberWithCommas(castAmount);
		};
		const updateRuneCounts = (actionID) => {
			if (!hasUI) return;

			let action = RUNE_ACTIONS[actionID];
			$(`#arc-view-have-${htmlID(action.id)}`).text(numberWithCommas(bankQty(action.product)));
			$(`#arc-view-cast-${htmlID(action.id)}`).text(runesCastFormat(action.product, actionID));
		};

		const updateResourceCounts = (item) => {
			if (!hasUI) return;

			$(`#arc-resource-have-${htmlID(item.id)}`).text(numberWithCommas(bankQty(item)));
		};

		// html helpers
		const CATEGORY_CLASS = {
			"melvorF:StandardRunes": "border-smithing",
			"melvorF:CombinationRunes": "border-crafting",
			"melvorItA:AbyssalRunes": "border-fletching",
			"melvorItA:AbyssalComboRunes": "border-firemaking",
		};
		const htmlID = (id) => id.replace(/[:_]/g, "-").toLowerCase();

		// ui
		const helpAlert = () => {
			SwalLocale.fire({
				title: `SEMI Auto Runecrafting`,
				width: '60em',
				html: `<div class="text-left font-size-sm"><span class="font-size-base font-w600 text-info">Crafting Modes</span>
				<span class="text-info"><b>Linear:</b></span>
				One rune at a time until it reaches Max Cast, then swaps to the next rune.

				<span class="text-info"><b>Lowest Cast:</b></span>
				Crafts the rune with the lowest amount, swapping if needed, until Max Cast.

				<span class="text-info"><b>Endless:</b></span>
				Same as Lowest Cast, but ignores Max Cast setting and goes forever.
				<hr><span class="font-size-base font-w600 text-info">Crafting Ratios & Cast Possible</span>
				The core of the mod is based around crafting ratios of runes based on the cast cost of a spell, for example:

				Spell "Fire Bolt" cost:
				<samp>3</samp> x <img src="assets/media/bank/rune_air.png" class="skill-icon-xxs"> Air Runes
				<samp>1</samp> x <img src="assets/media/bank/rune_chaos.png" class="skill-icon-xxs"> Chaos Rune
				<samp>4</samp> x <img src="assets/media/bank/rune_fire.png" class="skill-icon-xxs"> Fire Runes

				Putting the same numbers into the auto-crafting settings will cause the mod to craft 3 air runes and 4 fire runes for every 1 chaos.


				If using <span class="text-info">Linear</span> or <span class="text-info">Lowest Cast</span> crafting modes, this will set a upper limit to how many runes of a single type that will be crafted, based on the ratio.
				<i>(If the Max Cast value is 1000, and the current settings are 3:1:4 like above, the final runes crafted will be 3000 Air Runes, 1000 Chaos Runes, and 4000 Fire Runes)</i>


				If using <span class="text-info">Endless</span> mode, the mod will always select the rune that has the lowest amount of cast possible, in order to raise the max possible cast.
				This mode is also good for balanced crafting of all runes by placing 1 in every rune ratio setting.
				<hr><span class="font-size-base font-w600 text-info">Crafting Jobs</span>
				While base runes only require a type of rune essence, combo runes may require crafting of runes that aren't currently selected by the user to be crafted. In this case the mod will still craft the required runes automatically.

				In the event there is no possible action to take, the mod will resort to just mining <span class="text-warning">Rune Essence</span> if possible and the setting is enabled.
				<hr><span class="font-size-base font-w600 text-info">Sync Combat Runes & Reset</span>
				<span class="text-success">Sync Combat Runes</span> will populate the rune ratios with the the selected spells in the spellbook.
				
				<span class="text-danger">Reset</span> will set all craft ratios to <span class="text-info">0</span>.
                </div>`.replace(/\r\n|\r|\n/gi, "<br>")
			});
		};

		const setEnableButton = () => {
			$('#arc-toggle-enable').prop('checked', config.enabled);
		};

		const updateUI = (updateRunes = true, updateMining = true) => {
			if (!hasUI) return;

			if (updateRunes) {
				for (const action of RUNE_ACTIONS_ARRAY) {
					updateRuneCounts(action.id);
				}
			}
			if (updateMining) {
				for (const resource of MINING_RESOURCES_ARRAY) {
					updateResourceCounts(resource);
				}
			}
		}

		const injectUI = () => {
			if ($(`#auto-runecraft`).length) {
				return;
			}

			const createMiningResource = (action) => {
				return `
				<li class="block block-rounded-double bg-combat-inner-dark text-center pl-1 mr-1 ml-1" style="min-width: 150px;" id="arc-btn-resource-${htmlID(action.id)}">
					<div class="media d-flex align-items-center" style="line-height: 1.15;">
						<div class="mr-1">
							<img class="skill-icon-xs mr-1" src="${action.media}">
						</div>
						<div class="media-body text-left">
							<div><small class="font-w600 text-warning">${action.name}</small></div>
							<div><small class="font-w600 mr-1" id="arc-resource-have-${htmlID(action.id)}">0</small></div>
						</div>
					</div>
				</li>`;
			};

			const createRuneItem = (action) => {
				return `
				<div id="arc-btn-${htmlID(action.id)}" class="col-sm-6 col-md-3 col-xl-2">
					<div class="block block-rounded-double bg-combat-inner-dark pl-2 pr-2 pt-1 pb-2 border-top ${(CATEGORY_CLASS[action.category.id] || 'border-farming')
					} border-2x">
						<div class="row row-deck gutters-tiny">
							<div class="col-12">
								<small class="font-w700 text-left text-combat-smoke m-1">
									<img src="${action.recipe.media}" class="skill-icon-xs mr-1"> ${action.recipe.name}</small>
								</div>
							<div class="col-12">
								<input type="text" class="form-control form-control-small" id="arc-ratio-${htmlID(action.id)}" placeholder="0"></input>
							</div>
							<div class="col-12 small pt-1" style="text-align:center;">
								<div class="border-right border-smithing border-1x w-50 d-inline-block">
									Quantity:<br>
									<span id="arc-view-have-${htmlID(action.id)}">-</span>
								</div>
								<div class="w-50 d-inline-block">
									Cast Possible:<br>
									<span id="arc-view-cast-${htmlID(action.id)}">-</span>
								</div>
							</div>
						</div>
					</div>
				</div>`;
			};

			const autoRunecraftDiv = `
			<div class="w-100 d-flex flex-column">
				<div class="block-content pt-3">
					<div class="row row-deck gutters-tiny">
						<div class="col-sm-6 col-md-4">
							<div class="block block-rounded-double bg-combat-inner-dark p-3">
								<div class="row h-100 text-center align-items-center">
									<div class="custom-control custom-switch custom-control-lg w-100 mb-2">
										<input class="custom-control-input" type="checkbox" name="arc-toggle-enable" id="arc-toggle-enable">
										<label class="font-weight-normal ml-2 custom-control-label" for="arc-toggle-enable">Enable SEMI Auto Runecrafting</label>
									</div>
									<div class="w-100">
										<button class="btn btn-sm btn-success" type="button" id="arc-import-spells-btn">Sync Combat Runes</button>
										<button class="btn btn-sm btn-danger" type="button" id="arc-reset-btn">Reset</button>
										<button class="btn btn-sm btn-primary" type="button" id="arc-help-btn">Help</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="block block-rounded-double bg-combat-inner-dark p-3">
								<div class="border-bottom border-mining border-2x pb-2">Crafting Mode:</div>
								<div class="form-group pt-2 mb-0">
									<div id="arc-cmd-1" class="custom-control custom-radio custom-control-inline custom-control-lg">
										<input type="radio" class="custom-control-input" id="arc-crafting-mode-1" name="arc-craft-mode" value="1">
										<label class="custom-control-label" for="arc-crafting-mode-1">Linear</label>
									</div>
									<div id="arc-cmd-2" class="custom-control custom-radio custom-control-inline custom-control-lg">
										<input type="radio" class="custom-control-input" id="arc-crafting-mode-2" name="arc-craft-mode" value="2">
										<label class="custom-control-label" for="arc-crafting-mode-2">Lowest Cast</label>
									</div>
									<div id="arc-cmd-3" class="custom-control custom-radio custom-control-inline custom-control-lg">
										<input type="radio" class="custom-control-input" id="arc-crafting-mode-3" name="arc-craft-mode" value="3">
										<label class="custom-control-label" for="arc-crafting-mode-3">Endless Crafting</label>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="block block-rounded-double bg-combat-inner-dark p-3">
								<div class="border-bottom border-mining border-2x pb-2">Max Cast: <small>[Linear / Lowest Cast]</small></div>
								<div class="form-group pt-2 mb-0">
									<input type="text" class="form-control form-control-small" id="arc-cast-count" placeholder="10000"></input>
								</div>
							</div>
						</div>
					</div>
					<div class="row row-deck gutters-tiny">
						<div class="col-12">
							<ul class="nav-main nav-main-horizontal nav-main-hover nav-main-horizontal-center font-w400 font-size-sm w-100">
								${MINING_RESOURCES_ARRAY.map((miningItem) => createMiningResource(miningItem)).join('')}
							</ul>
						</div>
					</div>
					<div class="row row-deck gutters-tiny">
						${RUNE_ACTIONS_ARRAY.map((runeItem) => createRuneItem(runeItem)).join('')}
					</div>
				</div>
			</div>`;

			const inputDefaultFill = () => {
				$(`#arc-crafting-mode-${config.craftingMode}`).prop('checked', true);
				$('#arc-cast-count').val(config.baseCastCount);

				Object.keys(config.runeRatio).forEach((actionid) => {
					$(`#arc-ratio-${htmlID(actionid)}:text`).val(config.runeRatio[actionid]);
				})
			};

			const inputEventTriggers = () => {
				$(`#arc-help-btn`).on('click', function (e) {
					helpAlert();
				});

				$(`#arc-import-spells-btn`).on('click', function (e) {
					importSpells();
				});

				$(`#arc-reset-btn`).on('click', function (e) {
					resetRunes();
				});

				$('[name=arc-craft-mode]').on('click', function () {
					config.craftingMode = parseInt($(this).val());
					storeConfig();
				});
				Object.keys(config.runeRatio).forEach((actionid) => {
					$('#arc-ratio-' + htmlID(actionid)).on('input', function () {
						config.runeRatio[actionid] = parseInt($(this).val());
						updateRuneCounts(actionid);
						storeConfig();
					});
				});
				$('#arc-cast-count').on('input', function () {
					config.baseCastCount = parseInt($(this).val());
					storeConfig();
				});

				$(`#arc-toggle-enable`).on('change', function (e) {
					if (!game.runecrafting.isUnlocked) {
						$(this).prop('checked', false);
						config.enabled = false;
						storeConfig();
						return;
					}

					config.enabled = $(this).prop('checked');
					storeConfig();
					setEnableButton();
					resetScriptState();
					if (config.enabled) {
						nextScriptJob();
						runScriptJob();
					}
				});
			};

			const inputAddTippy = () => {
				[
					'One rune at a time until it reaches Max Cast, then swaps to the next rune.',
					'Crafts the rune with the lowest amount, swapping if needed, until Max Cast.',
					'Crafts the rune with the lowest amount, swapping if needed, but ignores Max Cast and goes forever.',
				].forEach((desc, i) => {
					tippy(`#arc-cmd-${i + 1}`, {
						content: `<div class="text-center">${desc}</div>`,
						placement: 'top',
						allowHTML: true,
						interactive: false,
						animation: false,
					});
				});
			};

			const container = createElement('div', {
				id: 'auto-runecraft',
				classList: ['col-12']
			});
			const holder = new CollapsibleMenu(container.appendChild(createElement('div', {
				classList: ['block']
			})), "Auto-Runecrafting Options");
			holder.toggle();

			holder.container.innerHTML = autoRunecraftDiv;
			$('#runecrafting-category-menu').after(container);

			inputDefaultFill();
			inputEventTriggers();
			inputAddTippy();
			updateUI();
			setEnableButton();
		}


		// script
		const scriptState = {};
		let LOGIC_TICK_COUNTER = 0;
		let LOGIC_MAX_TICKS = 250;
		let DO_SCRIPT_INTERACTION = false;

		const resetScriptState = () => {
			scriptState.job = null;
		};

		const scriptIsIdle = () => !game.mining.isActive && !game.runecrafting.isActive;

		const actionIsValid = (skill, action) => {
			return skill.level >= action.level && skill.abyssalLevel >= action.abyssalLevel && action.realm.isUnlocked && skill.isUnlocked;
		}

		const miningActionValid = (miningAction) => {
			return (game.bank.getQty(miningAction.action.product) >= miningAction.cost || actionIsValid(MINING_SKILL, miningAction.action));
		};

		/** For a given action, figure out the job to start, or no job is uncompletable. **/
		const calculateBestJob = (mainAction) => {
			if (!mainAction)
				return null;

			const stepCalculate = (runeAction) => {
				let canCraft = true,
					miningAction = null,
					craftRune = null;

				if (!mainAction)
					return {
						canCraft,
						miningAction,
						craftRune
					};

				// Iterate Through Item Cost
				let foundAction,
					foundMining;

				let costs = runeAction.recipe.itemCosts;
				for (let i = 0; i < costs.length; i++) {
					let {
						item,
						quantity
					} = costs[i];
					let runeQty = bankQty(item);

					if (runeQty > quantity) {
						debugLog('have enough', item.name, runeQty, quantity);
						continue;
					} else if ((foundAction = RUNE_ACTIONS_ARRAY.find((action) => action.product == item))) {
						debugLog('foundAction', foundAction.id, RC_SKILL.id, foundAction.id);
						canCraft = false;
						if (actionIsValid(RC_SKILL, foundAction)) {
							craftRune = foundAction;
						}
						break;
					} else if ((foundMining = MINING_ACTIONS.find((action) => action.product == item))) {
						debugLog('foundMining', foundMining.id, MINING_SKILL.id, foundMining.id);
						canCraft = false;
						if (actionIsValid(MINING_SKILL, foundMining)) {
							miningAction = foundMining;
						}
						break;
					} else {
						debugLog('unknownItemSource', item);
						canCraft = false;
						break;
					}
				}

				return {
					canCraft,
					miningAction,
					craftRune
				};
			};

			let canCraft,
				miningAction,
				craftRune;
			let bestAction = mainAction;

			for (let i = 0; i < 10; i++) { // check down to 10 depths
				debugLog('depth', i);
				({
					canCraft,
					miningAction,
					craftRune
				} = stepCalculate(bestAction));

				if (!canCraft && !miningAction && !craftRune) {
					debugLog('recipe is uncompletable...');
					return null;
				}

				if (canCraft) {
					debugLog('best to craft rune:', bestAction);
					return {
						skill: RC_SKILL,
						action: bestAction.recipe
					};
				}
				if (miningAction) {
					debugLog('best to start mining:', miningAction);
					return {
						skill: MINING_SKILL,
						action: miningAction
					};
					break;
				}
				if (craftRune) {
					debugLog('need to check lower:', craftRune);
					bestAction = craftRune;
					continue;
				}
			}

			return null;
		};

		/** For top level best rune to craft **/
		const calculateBestAction = () => {
			let limitedCrafting = config.craftingMode == MODE_LINEAR || config.craftingMode == MODE_LOWEST_CAST;

			// Find lowest rune
			let bestAction = null;
			let lowestCast = Number.POSITIVE_INFINITY;
			for (const action of RUNE_ACTIONS_ARRAY) {
				let runeCastable = runesCast(action.product, action.id);
				if (runeCastable >= 0) {
					//debugLog("Checking:", action.id);
					if (actionIsValid(RC_SKILL, action)) {
						// max crafting requirement reached, skipping
						if (limitedCrafting && runeCastable >= config.baseCastCount) {
							debugLog("Reached max requested cast amounts.");
							continue; // reached max requested cast amounts
						}

						// essence check + level requirement
						if (!action.mining.every(miningActionValid)) {
							debugLog("Skipping due to mining action.");
							continue; // no essence or way to acquire more.
						}

						if (!bankHasRoom(action.product)) {
							debugLog("Skipping as not enough bank space for product.");
							continue; // no space in bank.
						}

						// currently not needed, only main failure point is pure essence mining requirement in base game.
						// mods may add additional recipes that add a non-mining/non-runecrafting itemCost that this should handle.
						/*
						if(!calculateBestJob(action)) {
						debugLog("Skipping due to crafting isn't completable at some step.");
						continue;
						}
						 */

						// new lowest to craft
						if (runeCastable < lowestCast) {
							//debugLog("New Lowest:", runeCastable, lowestCast);
							lowestCast = runeCastable;
							bestAction = action;
						}

						// found a craftable rune, continue
						if (config.craftingMode == MODE_LINEAR || lowestCast <= 0) {
							break;
						}
					} else {
						debugLog(`${RC_SKILL.level} / ${action.level} && ${RC_SKILL.abyssalLevel} / ${action.abyssalLevel} && ${action.realm.isUnlocked}`);
					}
				}
			}
			return bestAction;
		}

		const nextScriptJob = () => {
			let bestAction = calculateBestAction();
			let job = calculateBestJob(bestAction);

			if (job) {
				scriptState.job = job;
				return;
			}

			if (!MINING_SKILL.isUnlocked) {
				shutDown("Mining skill is locked. Shutting down.");
				return;
			}

			if (!SETTINGS.get('mineRuneEssence')) {
				shutDown("Mining Rune Essence is disabled in settings. Shutting down.");
				return;
			}

			debugLog("No Job to complete, mining Rune Essence.");
			const miningAction = MINING_ACTIONS.find(
				(mc_action) => mc_action.product === game.items.getObjectByID('melvorD:Rune_Essence')
			);

			if (!miningAction) {
				shutDown("No valid mining action found for Rune Essence. Shutting down.");
				return;
			}

			if (!bankHasRoom(miningAction.product)) {
				shutDown("No job and no room for Rune Essence. Shutting down.");
				return;
			}

			scriptState.job = {
				skill: game.mining,
				action: miningAction
			};

		};

		const runScriptJob = () => {
			if (config.enabled && scriptState.job) {
				const isOfflineProgress = loadingOfflineProgress;
				//debugLog('run job', scriptState.job);

				let {
					skill,
					action
				} = scriptState.job;

				if (skill == MINING_SKILL) {
					if (!game.mining.isActive || game.mining.selectedRock != action) {
						debugLog('Starting Mining Job:', action.name, ", isOffline:", isOfflineProgress);

						if (isOfflineProgress) inCharacterSelection = true;
						DO_SCRIPT_INTERACTION = true;
						game.mining.onRockClick(action);
						if (isOfflineProgress) inCharacterSelection = false;
					}
				} else if (skill == RC_SKILL) {
					if (!game.runecrafting.isActive || game.runecrafting.selectedRecipe != action) {
						debugLog('Starting Runecrafting Job:', action.name, ", isOffline:", isOfflineProgress);

						if (isOfflineProgress) inCharacterSelection = true;
						DO_SCRIPT_INTERACTION = true;
						game.runecrafting.selectRecipeOnClick(action);
						game.runecrafting.createButtonOnClick();
						if (isOfflineProgress) inCharacterSelection = false;
					}
				} else {
					debugLog('Unknown Skill Job:', skill.name);
				}
			}
		};

		const runLogicTick = () => {
			LOGIC_TICK_COUNTER++;
			//debugLog('LOGIC_TICK_COUNTER:', LOGIC_TICK_COUNTER);

			if (LOGIC_TICK_COUNTER > LOGIC_MAX_TICKS) {
				LOGIC_TICK_COUNTER = 0;
				nextScriptJob();
			}
		};

		const shutDown = (txt) => {
			notifyPlayer(game.runecrafting, `[Auto-Runecrafting] ${txt}`, 'danger');
			debugLog(txt);
			config.enabled = false;
			storeConfig();
			setEnableButton(config.enabled);
		};

		// patch function callbacks
		const onMiningAction = () => {
			//debugLog('onMiningAction', game.runecrafting.isActive, game.mining.isActive);

			if (config.enabled) {
				updateUI(false);

				if (scriptIsIdle()) {
					LOGIC_TICK_COUNTER = 0;
					//console.log("Calculate New Job, Idle");
					nextScriptJob();
				}

				if (game.mining.isActive) {
					runLogicTick();
				}

				runScriptJob();
			}
		};

		const onRunecraftingAction = () => {
			//debugLog('onRunecraftingAction', game.runecrafting.isActive, game.mining.isActive);
			updateUI();

			if (config.enabled) {
				if (scriptIsIdle()) {
					LOGIC_TICK_COUNTER = 0;
					//console.log("Calculate New Job, Idle");
					nextScriptJob();
				}

				if (game.runecrafting.isActive) {
					runLogicTick();
				}

				runScriptJob();
			}
		};

		const importSpells = () => {
			const runeCost = Object.values(game.combat.player.spellSelection).filter(spell => spell?.runesRequired).flatMap(spell => game.combat.player.getRuneCosts(spell));
			const costs = new Map();
			const actions = {};

			// Merge Spell Cost
			runeCost.forEach(rune => {
				costs.set(rune.item, ((costs.get(rune.item) || 0) + rune.quantity));
			});

			// Reset Config to 0
			Object.keys(RUNE_ACTIONS).forEach(actionID => {
				actions[actionID] = 0;
			});

			// Set Rune Spell Cost
			costs.forEach((cost, rune) => {
				const action = RUNE_ACTIONS_ARRAY.find(a => a.product == rune); // Find action related to rune.
				if (action)
					actions[action.id] = cost;
			});

			// Update Config
			Object.keys(RUNE_ACTIONS).forEach(actionID => {
				config.runeRatio[actionID] = actions[actionID];
				updateRuneCounts(actionID);
				$('#arc-ratio-' + htmlID(actionID)).val(actions[actionID]);
			});

			storeConfig();
		};

		const resetRunes = () => {
			Object.keys(RUNE_ACTIONS).forEach((actionID) => {
				config.runeRatio[actionID] = 0;
				updateRuneCounts(actionID);
				$('#arc-ratio-' + htmlID(actionID)).val(0);
			});
			storeConfig();
		};

		SETTINGS.add({
			'type': 'switch',
			'name': `mineRuneEssence`,
			'label': `Mine Rune Essence`,
			'hint': `If no runes can be crafted, should Rune Essence be mined?`,
			'default': true
		});

		// hooks + game patches
		ctx.onCharacterLoaded((ctx) => {
			// load config
			config = getConfig();

			// game patches
			ctx.patch(Mining, 'action').after(() => {
				onMiningAction();
			});

			ctx.patch(Runecrafting, 'action').after(() => {
				onRunecraftingAction();
			});

			// disable script if interrupted
			ctx.patch(Mining, 'onRockClick').before(() => {
				if (!config.enabled)
					return;

				if (!DO_SCRIPT_INTERACTION)
					shutDown("Player has interrupted runecrafting, stopping.");

				DO_SCRIPT_INTERACTION = false;
			});

			ctx.patch(Runecrafting, 'selectRecipeOnClick').before(() => {
				if (!config.enabled)
					return;

				if (!DO_SCRIPT_INTERACTION)
					shutDown("Player has interrupted runecrafting, stopping.");

				DO_SCRIPT_INTERACTION = false;
			});
		});

		ctx.onInterfaceReady(() => {
			hasUI = true;
			LOGIC_MAX_TICKS = 20;
			injectUI();
		});
	});
}